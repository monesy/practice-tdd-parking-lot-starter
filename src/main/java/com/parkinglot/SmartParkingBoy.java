package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoy implements ParkingBoy{
    private List<ParkingLot> parkingLots = new ArrayList<>();

    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
        int maxPosition = 0;
        ParkingLot maxParkingLot = new ParkingLot();
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.getRestPosition() > maxPosition)
                maxPosition = parkingLot.getRestPosition();
            maxParkingLot = parkingLot;
        }
        if (maxPosition == 0) {
            throw new NoAvailablePositionException();
        } else {
            return maxParkingLot.park(car);
        }
    }


    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : parkingLots) {
            if(parkingLot.matchCarByParkingLot(ticket)) {
                return parkingLot.fetch(ticket);
            }
        }
        throw new UnrecognizedTicketException();
    }
}
