package com.parkinglot;

public class NoParkingBoyAvailableException extends RuntimeException {
    public NoParkingBoyAvailableException(){
        super("No Parking Boy Available. ");
    }
}
