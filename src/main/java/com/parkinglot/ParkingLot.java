package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {

    public static final int MAX = 10;
    private HashMap<Ticket, Car> ticketCarMap = new HashMap<>();
    private int restPosition = MAX;

    public void setRestPosition(int restPosition) {
        this.restPosition = restPosition;
    }

    public int getRestPosition() {
        return restPosition;
    }

    public boolean matchCarByParkingLot(Ticket ticket){
        return this.ticketCarMap.containsKey(ticket);
    }

    public Ticket park(Car car) {
        if (this.restPosition < 1) {
            throw new NoAvailablePositionException();
        } else {
            Ticket ticket = new Ticket();
            this.ticketCarMap.putIfAbsent(ticket, car);
            this.restPosition--;
            return ticket;
        }

    }

    public Car fetch(Ticket ticket) {
        if (!ticketCarMap.containsKey(ticket)) {
            throw new UnrecognizedTicketException();
        } else {
            Car car = this.ticketCarMap.getOrDefault(ticket, null);
            this.ticketCarMap.remove(ticket);
            this.restPosition++;
            return car;
        }

    }

    public double getAvailablePositionRate(){
        return (double) getRestPosition()/MAX;
    }
}
