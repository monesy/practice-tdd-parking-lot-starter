package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingLotServiceManager {
    private List<ParkingLot> parkingLots = new ArrayList<>();
    private List<ParkingBoy> parkingBoys = new ArrayList<>();

    public ParkingLotServiceManager() {
    }


    public void addParkingBoy(ParkingBoy... parkingBoy) {
        parkingBoys.addAll(List.of(parkingBoy));

    }

    public Ticket park(Car car) {
        return parkingLots.stream()
                .filter(parkingLot->parkingLot.getRestPosition()>0)
                .findFirst()
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }
}
