package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SuperSmartParkingBoy implements ParkingBoy{
    private List<ParkingLot> parkingLots = new ArrayList<>();

    public SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
        double maxPositionRate = 0;
        ParkingLot maxRateParkingLot = new ParkingLot();
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.getAvailablePositionRate() > maxPositionRate)
                maxPositionRate = parkingLot.getAvailablePositionRate();
                maxRateParkingLot = parkingLot;
        }
        if (maxPositionRate == 0) {
            throw new NoAvailablePositionException();
        } else {
            return maxRateParkingLot.park(car);
        }
    }


    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : parkingLots) {
            if(parkingLot.matchCarByParkingLot(ticket)) {
                return parkingLot.fetch(ticket);
            }
        }
        throw new UnrecognizedTicketException();
    }
}
