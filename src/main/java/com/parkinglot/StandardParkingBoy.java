package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class StandardParkingBoy implements ParkingBoy{
    private List<ParkingLot> parkingLots = new ArrayList<>();

    public StandardParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {

        return parkingLots.stream()
                .filter(parkingLot->parkingLot.getRestPosition()>0)
                .findFirst()
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);

    }

    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : parkingLots) {
            if(parkingLot.matchCarByParkingLot(ticket)) {
                return parkingLot.fetch(ticket);
            }
        }
            throw new UnrecognizedTicketException();



    }
}
