package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ParkingLotServiceManagerTest {
    @Test
    void should_return_ticket_when_park_car_given_car_and_parking_manager() {
        //give
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingLot parkingLot3 = new ParkingLot();
        ParkingLot parkingLot4 = new ParkingLot();
        ParkingLot parkingLot5 = new ParkingLot();
        ParkingLot parkingLot6 = new ParkingLot();
        parkingLot1.setRestPosition(2);
        parkingLot2.setRestPosition(2);
        parkingLot3.setRestPosition(2);
        parkingLot4.setRestPosition(2);
        parkingLot5.setRestPosition(2);
        parkingLot6.setRestPosition(2);

        StandardParkingBoy parkingBoy = new StandardParkingBoy(List.of(parkingLot1,parkingLot2));
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot3,parkingLot4));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot5,parkingLot6));

        parkingLotServiceManager.addParkingBoy(parkingBoy,smartParkingBoy,superSmartParkingBoy);

        Car car = new Car();
        //when
        Ticket ticket = parkingLotServiceManager.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }
}
