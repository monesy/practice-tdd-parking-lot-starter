package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SuperSmartStandardParkingBoyTest {
    @Test
    void should_return_ticket_and_parking_lot1_rest_less1_when_park_car_given_car_and_two_parking_lot_and_super_smart_parking_boy() {
        //give
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot2.setRestPosition(5);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car = new Car();

        //when
        Ticket ticket = superSmartParkingBoy.park(car);

        //then
        Assertions.assertNotNull(ticket);
        Assertions.assertEquals(parkingLot1.getRestPosition(),9);
    }

    @Test
    void should_return_car_when_fetch_car_given_ticket_and_two_parking_lot_and_super_smart_parking_boy() {

        ParkingLot parkingLot = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car = new Car();
        Ticket ticket = superSmartParkingBoy.park(car);

        Car resultCar = superSmartParkingBoy.fetch(ticket);

        Assertions.assertEquals(car, resultCar);
    }

    @Test
    void should_return_right_car_when_fetch_car_given_two_ticket_and_two_parking_lot_and_super_smart_parking_boy() {


        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setRestPosition(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);

        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = superSmartParkingBoy.park(car1);
        Ticket ticket2 = superSmartParkingBoy.park(car2);

        Car resultCar1 = superSmartParkingBoy.fetch(ticket1);
        Car resultCar2 = superSmartParkingBoy.fetch(ticket2);

        Assertions.assertEquals(car1, resultCar1);
        Assertions.assertEquals(car2, resultCar2);
    }

    @Test
    void should_return_nothing_and_give_message_when_fetch_car_given_wrong_ticket_and_two_parking_lot_and_super_smart_parking_boy() {
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        Ticket ticket = new Ticket();


        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> superSmartParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_nothing_when_fetch_car_given_used_ticket_and_two_parking_lot_and_super_smart_parking_boy() {
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car = new Car();
        Ticket ticket = superSmartParkingBoy.park(car);
        superSmartParkingBoy.fetch(ticket);


        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> superSmartParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_nothing_and_give_message_when_park_car_given_full_parking_lot_and_super_smart_parking_boy() {
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        for (ParkingLot parkingLot : parkingLots) {
            parkingLot.setRestPosition(0);
        }
        Car car = new Car();

        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, () -> superSmartParkingBoy.park(car));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());
    }
}
