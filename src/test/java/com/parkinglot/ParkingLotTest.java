package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_car_given_car_and_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        Ticket ticket = parkingLot.park(car);

        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_car_given_ticket_and_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);

        Car resultCar = parkingLot.fetch(ticket);

        Assertions.assertEquals(car, resultCar);

    }

    @Test
    void should_return_right_car_when_fetch_car_given_two_ticket_and_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        Car resultCar1 = parkingLot.fetch(ticket1);
        Car resultCar2 = parkingLot.fetch(ticket2);

        Assertions.assertEquals(car1, resultCar1);
        Assertions.assertEquals(car2, resultCar2);
    }

    @Test
    void should_return_nothing_and_give_message_when_fetch_car_given_wrong_ticket_and_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = new Ticket();


        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_nothing_when_fetch_car_given_used_ticket_and_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);


        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_nothing_and_give_message_when_park_car_given_full_parking_lot() {
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setRestPosition(0);
        Car car = new Car();

        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(car));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());
    }


}
